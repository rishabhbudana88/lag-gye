from django import forms
from django.forms import widgets
from .models import Image

class ImageForm(forms.ModelForm):
    class Meta:
        model=Image
        fields=("caption","reg","hog","hogph","address","state","city","pin","name","phone","ifsc","accno","image")
        labels = {
            'caption':'Name Of Organisation ',
            'reg' : 'License Number ',
            'hog': 'Head Of Organisation ',
            'hogph': 'Mobile No. ',
            'address': 'Address ',
            'state': 'State ',
            'city': 'City ',
            'pin': 'PIN Code ',
            'name': 'Year Of Establishment ',
            'phone': 'NGO Phone No. ',
            'ifsc': 'IFSC Code ',
            'accno': 'Account Number ',
            'image': 'NGO Display Picture ',

        }
        # widgets = {
        #     'pin': forms. 
        # }