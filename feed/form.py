from django import forms
from .models import Postimg

class ImageForm(forms.ModelForm):
    class Meta:
        model=Postimg
        fields=("caption","image")